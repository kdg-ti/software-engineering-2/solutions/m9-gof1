import filesystem.*;
import user.User;

/* OPGAVE: Hanteer het Composite pattern om volgende opgave uit te werken:
 * Gegeven: de abstracte klasse Component en de klasse Client.
 * Gevraagd: werk de klassen File en Directory uit. Beide klassen gebruiken de
 * klasse Component. Een Directory-object kan andere Componenten bevatten;
 * een File-object natuurlijk niet.
 * De verwachte afdruk staat onderaan.
*/

public class Client {
  public static void main(String[] args) {
    Directory dir1 = new Directory("C:");
    Directory dir2 = new Directory("My Documents");
    Directory dir3 = new Directory("Word");
    Directory dir4 = new Directory("Excel");
    File file1 = new File("Document1", 250);
    File file2 = new File("Picture1", 410);
    File file3 = new File("Sheet1", 175);

    dir1.add(dir2);
    dir1.add(file1);
    dir2.add(dir3);
    dir2.add(dir4);
    dir3.add(file1);
    dir4.add(file3);
    dir2.add(file2);

    User jan = new User("Jan");
    Directory backup = new Directory("Backup");
    dir1.add(backup);
    file1.subscribe(jan);
    file1.subscribe(backup);

    file1.save(200);
    dir1.remove(file1);

    System.out.println("Files in history :"+  backup.list());
    System.out.println(jan.getName() + "'s messages: " + jan.getMessages());

  }
}
