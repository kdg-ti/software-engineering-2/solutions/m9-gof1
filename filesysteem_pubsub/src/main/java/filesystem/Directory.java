package filesystem;

import publisher.Subscriber;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public  class Directory extends File implements Container, Subscriber {
  private final List<Component> children;

  public Directory(String name){
  	super(name,0);
    children = new ArrayList<>();
  }

	@Override
	public long getSize() {
		// TODO: implementeer methode

		long size = 0;
		for (Component child:children){
			size+=child.getSize();
		}
		return size;
	}

	public void add(Component c) {
		// TODO: implementeer methode

		children.add(c);
		c.setParent(this);
	}

	public void remove(Component c) {
		// TODO: implementeer methode

		children.remove(c);
		c.setParent(null); //Wordt vaak vergeten!
	}

	@Override
	public void update(String event, Object source) {
		File file = (File) source;
		add(new File(file.getName()+"."+ LocalDateTime.now(), file.getSize()));
	}

	public List<Component> list() {
		return children;
	}
}
