package filesystem;

import publisher.Publisher;
import publisher.Subscriber;

import java.util.ArrayList;
import java.util.List;

public class File implements Component, Publisher {
	private String name;
	private long size;
	private Container parent;
	private List<Subscriber> subscribers = new ArrayList<>();

	public File(String name, long size) {
		this(name, size, null);
	}

		public File(String name, long size,Container parent) {
		this.name = name;
		this.size = size;
		this.parent = parent;
	}

	@Override
	public long getSize() {
		// TODO: implementeer methode
		return size;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getPath() {
		// TODO: implementeer methode
		return (parent == null ? "" : parent.getPath()) + "/" + name;
	}

	@Override
	public void setParent(Container parent) {
		File original=null;
		if (parent == null) {
			 original = copy();
		}
		this.parent = parent;
		if (parent == null) {
			notifySubscribers("removed",original);
		}
	}

	@Override
	public String toString() {
		// TODO: implementeer methode
		return getPath() + "(" + getSize() + "kb)";
	}

	public void save(int size) {
		File original = copy();
		this.size = size;
		notifySubscribers("saved",original);
	}

	private void notifySubscribers(String event, File original) {
		for (Subscriber subscriber : subscribers) {
			subscriber.update(event, original);
		}
	}

	@Override
	public void subscribe(Subscriber subscriber) {
		subscribers.add(subscriber);
	}

	@Override
	public void unsubscribe(Subscriber subscriber) {
		subscribers.remove(subscriber);
	}

	private File copy() {
		File clone = new File(name, size);
		clone.parent = parent;
		return clone;
	}
}
