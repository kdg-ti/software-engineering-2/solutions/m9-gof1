package publisher;

public interface Subscriber {
    void update(String event, Object source);
}
