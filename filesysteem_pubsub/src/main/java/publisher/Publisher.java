package publisher;

public interface Publisher {
     void subscribe(Subscriber subscriber);
    void unsubscribe(Subscriber subscriber);

}
