package user;

import filesystem.File;
import publisher.Subscriber;

import java.util.*;

public class User implements Subscriber {
	private String name;
	private List<String> messages=new LinkedList<>();

	public User(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void addMessage(String message) {
		messages.addFirst(message);
	}

	@Override
	public void update(String event, Object source) {
		addMessage(event + " " + ((File)source).getPath());
	}

	public List<String> getMessages() {
		// messages are returned newest first
		return messages;
	}

	@Override
	public String toString() {
		return "User{" +
			"name='" + name + '\'' +
			", messages=" + messages +
			'}';
	}
}
