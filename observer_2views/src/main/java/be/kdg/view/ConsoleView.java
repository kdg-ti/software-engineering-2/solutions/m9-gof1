package be.kdg.view;

import be.kdg.model.AModel;
import be.kdg.model.BModel;

import java.util.Observable;
import java.util.Observer;

/**
 * Mark Goovaerts
 * 7/10/2016
 */
public class ConsoleView implements Observer {
    private AModel aModel;
    private BModel bModel;

    public ConsoleView(AModel aModel, BModel bModel) {
        this.aModel = aModel;
        this.bModel = bModel;
        aModel.addObserver(this);
        bModel.addObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        if(o instanceof AModel) {
            System.out.println("De waarde van a werd gewijzigd in: " + aModel.getA());
        }
        else if(o instanceof BModel) {
            System.out.println("De waarde van b werd gewijzigd in: " + bModel.getB());
        }
    }
}
