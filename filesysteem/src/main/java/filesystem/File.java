package filesystem;

public class File implements Component{
  private final String name;
  private final long size;
  private Directory parent;

  public File(String name, long size){
    this.name = name;
    this.size = size;
    this.parent = null;
  }

	@Override
	public long getSize() {
		// TODO: implementeer methode
		return size;
	}

	@Override
	public String getPath() {
		// TODO: implementeer methode
		return (parent == null?"":parent.getPath()) + "/"+name;
	}

	@Override
	public void setParent(Directory parent) {
		// TODO: implementeer methode
		this.parent=parent;
	}

	@Override
	public String toString() {
		// TODO: implementeer methode
		return getPath() + "(" +getSize() +"kb)";
	}

}
