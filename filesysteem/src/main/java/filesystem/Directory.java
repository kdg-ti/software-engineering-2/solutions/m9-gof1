package filesystem;

import java.util.*;

public final class Directory extends File implements Container{
  private final List<Component> children;

  public Directory(String name){
  	super(name,0);
    children = new ArrayList<>();
  }

	@Override
	public long getSize() {
		// TODO: implementeer methode

		long size = 0;
		for (Component child:children){
			size+=child.getSize();
		}
		return size;
	}

	public void add(Component c) {
		// TODO: implementeer methode

		children.add(c);
		c.setParent(this);
	}

	public void remove(Component c) {
		// TODO: implementeer methode

		children.remove(c);
		c.setParent(null); //Wordt vaak vergeten!
	}

}
